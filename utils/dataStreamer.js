var fs = require('fs');
var rl = require('readline');
var heapdump = require('heapdump');

//Get a process snapshot
heapdump.writeSnapshot();

var DataStream = (function() {
  DataStream.inputStream;

  DataStream.lineStream;

  function DataStream(filePath) {
    this.filePath = filePath;
    this.inputStreamOptions = {
      flags: 'r',
      encoding: "utf-8",
      fd: null,
      mode: "0666",
      bufferSize: 4 * 1024
    };
    this.lineStreamOptions = {
      input: this.createInputStream(),
      output: process.stdout,
      terminal: false
    };
    this.createLineStream();
  }

  DataStream.prototype.createInputStream = function() {
    return this.inputStream = fs.createReadStream(this.filePath, this.inputStreamOptions);
  };

  DataStream.prototype.createLineStream = function() {
    return this.lineStream = rl.createInterface(this.lineStreamOptions);
  };

  DataStream.prototype.writeLine = function(string) {
    return fs.appendFileSync(this.filePath, string);
  };

  DataStream.prototype.onOpen = function(callback) {
    return this.inputStream.on('open', function() {
      return callback();
    });
  };

  DataStream.prototype.onError = function(callback) {
    return this.inputStream.on('error', function(error) {
      return callback(error);
    });
  };

  DataStream.prototype.onData = function(callback) {
    return this.lineStream.on('line', function(line) {
      return callback(line);
    });
  };

  return DataStream;

})();

module.exports = DataStream;