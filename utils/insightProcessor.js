var Collection = require('gauss').Collection;
var _ = require('lodash');

InsightProcessor = (function() {


  function InsightProcessor(TAU) {
    this.TAU = TAU;
    this.data = new Collection();
  }

  InsightProcessor.prototype.getData = function() {
    return this.data;
  };

  InsightProcessor.prototype.processData = function(timestamp, value) {
    this.insertData(timestamp, value);
    this.removeUnnecesaryData();
    return this.processRow();
  };

  InsightProcessor.prototype.insertData = function(timestamp, value) {
    var row;
    this.currentTimestamp = timestamp;
    this.currentValue = value;
    row = {
      timestamp: this.currentTimestamp,
      value: this.currentValue
    };
    return this.data.push(row);
  };

  InsightProcessor.prototype.processRow = function() {
    var result;
    result = {
      timestamp: this.currentTimestamp,
      value: this.currentValue,
      min: _.min(this.data, 'value').value.toFixed(5),
      max: _.max(this.data, 'value').value.toFixed(5),
      sum: _.reduce(this.data, function(sum, row) {
        return sum + row.value;
      }, 0).toFixed(5),
      nb: this.data.length
    };
    return result;
  };

  InsightProcessor.prototype.removeUnnecesaryData = function() {
    var lastIndex, minTimestamp;
    minTimestamp = this.currentTimestamp - this.TAU;
    lastIndex = this.data.indexBy(function(row) {
      return row.timestamp > minTimestamp;
    });
    return this.data.splice(0, lastIndex);
  };

  return InsightProcessor;

})();

module.exports = InsightProcessor;