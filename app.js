var _ = require('lodash');
var fs = require('fs');
var DataStream = require('./utils/dataStreamer');
var InsightProcessor = require('./utils/insightProcessor');
var inputFile = './info/data.txt';
var outputFile ='./output/insights.txt';

fs.exists(outputFile,function(exists){
  if (exists){
    fs.unlink(outputFile);
  }
  fs.writeFile(outputFile,
      "Time      " + " "
      + "Value" + " "
      + "N_0" + " "
      + "Roll_Sum" + " "
      + "Min_Value" + " "
      + "Max_Value" + " \n" +
    "------------------------------------------------------------------\n",
    function (err) {
    if (err) throw err;

      var inputStream = new DataStream(inputFile);
      var outputStream = new DataStream(outputFile);

      var insightProcessor = new InsightProcessor(60);

      inputStream.onData(function(eachLine) {
        var processedData, values;
        values = eachLine.split(/\s/g);
        processedData = insightProcessor.processData(values[0], parseFloat(values[1]));
        return outputStream.writeLine(beautifyOutput(processedData));
      });


  });

});


/*********************** Helpers *************************/
var beautifyOutput = function(processedOutput) {
  return processedOutput.timestamp + " "
    + processedOutput.value + " "
    + processedOutput.nb + " "
    + (processedOutput.sum.toString()) + " "
    + (processedOutput.min.toString()) + " "
    + (processedOutput.max.toString()) + " \n";
};



